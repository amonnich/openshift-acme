# OpenShift-ACME

This is a CERN fork of https://github.com/tnozicka/openshift-acme which sets the correct annotation
on the validation route to expose it to the Internet.


## To deploy on OpenShift:

### 1. Add this to your project

#### Staging
```bash
oc create -fhttps://gitlab.cern.ch/amonnich/openshift-acme/raw/master/deploy/letsencrypt-staging/single-namespace/{role,serviceaccount,imagestream,deployment}.yaml
oc policy add-role-to-user openshift-acme --role-namespace="$(oc project --short)" -z openshift-acme
```

#### Production
```bash
oc create -fhttps://gitlab.cern.ch/amonnich/openshift-acme/raw/master/deploy/letsencrypt-live/single-namespace/{role,serviceaccount,imagestream,deployment}.yaml
oc policy add-role-to-user openshift-acme --role-namespace="$(oc project --short)" -z openshift-acme
```

### 2. Update your router
```bash
oc patch route YOUR-ROUTER-NAME -p '{"metadata":{"annotations":{"kubernetes.io/tls-acme":"true"}}}'
```



## To build the image:

You do not need this unless you are me, or want to use your own fork.

```bash
docker build -t gitlab-registry.cern.ch/amonnich/openshift-acme -f images/openshift-acme-controller/Dockerfile .
docker push gitlab-registry.cern.ch/amonnich/openshift-acme
```
